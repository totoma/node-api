const User = require('../models/User').User;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

require('dotenv').config();

exports.signUp = (req, res) => {
    const email = req.body.email;
    const name = req.body.name;
    const password = req.body.password;
    User.find({ email: email }).then(user => {
        if (user.length > 0) {
            res.status(503).json({ message: "L'email est déja existant" });
        } else {
            bcrypt.hash(password, 12).then(hashedPassword => {
                const newUser = new User({
                    email: email,
                    name: name,
                    password: hashedPassword
                })

                newUser.save().then(user => {
                    res.status(200).json({ user: user })
                }).catch(err => res.status(500).json({ message: err }));
            })
        }
    })
}

exports.signIn = (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({ email: email }).then(user => {
        console.log(user);
        bcrypt.compare(password, user.password).then(isEqual => {
            if (isEqual) {
                const token = jwt.sign({
                    email: user.email,
                    userId: user._id.toString(),
                    role: user.role
                },
                    process.env.JWT_SECRET_TOKEN,
                    {
                        expiresIn: '6h'
                    });

                const id = user.id
                const update = { token_valid: token }
                User.findByIdAndUpdate(id, update, function (error, response) {
                    if (error) {
                        console.log(error)
                    } else {
                        console.log(response);
                    }
                })
                res.status(200).json({ token: token, userEmail: user.email })
            } else {
                res.status(503).json({ message: "Le mot de passe est incorrect." })
            }

        }).catch(err => res.status(400).json({ message: "Une erreur s'est produite.", err: err }));
    }).catch(err => {
        res.status(503).json({ message: "L'email n'existe pas." + err });
    })
}

exports.getAllUsers = (req, res) => {
    User.find().then(promesse_user => {
        res.status(200).json({
            message: "liste de tous les utilisateurs.",
            user: promesse_user
        })
    }).catch(err => {
        res.status(400).json({
            message: "Une erreur s'est produite lors de la séléction de tous les utilisateurs.",
            err: err
        })
    })
}

exports.getUsers = (req, res) => {
    User.findById(req.params.id, {
    }).then(promesse_user => {
        res.status(200).json({
            message: "l'utilisateur a bien été trouvé.",
            user: promesse_user
        })
    }).catch(err => {
        res.status(500).json({
            message: "Une erreur est survenue lors de la recherche de l'utilisateur.",
            err: err
        })
    })
}

exports.update = (req, res) => {
    User.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role
    }).then(promesse_user => {
        res.status(200).json({
            message: "l'utilisateur a bien été mit à jour.",
            user: promesse_user
        })
    }).catch(err => {
        res.status(500).json({
            message: "erreur lors de la mise à jour de l'utilisateur.",
            err: err
        })
    })
}

exports.deleteUser = (req, res) => {
    const deleteById = req.params.id
    User.findByIdAndDelete(deleteById)
        .then(User => {
            res.status(201).json({
                message: "l'utilisateur a bien été supprimé.",
                user: User
            })
        }).catch(err => res.status(500).json({
            err: err
        }))
}