const router = require('express').Router()
const MangaTypeController = require('../controllers/Type_manga_Controller');

router.post('/createTypeManga', MangaTypeController.createTypeMangas);
router.get('/getAllTypeMangas', MangaTypeController.getAllTypeMangas);
router.put('/updateTypeManga/:id', MangaTypeController.updateTypeMangas);
router.delete('/deleteTypeMangas/:id', MangaTypeController.deleteTypeMangas);

module.exports = router;