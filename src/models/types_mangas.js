const mongoose = require('mongoose');

const TypeMangaSchema = new mongoose.Schema({
    name: { type: String, required: true }
})

module.exports.TypeMangas = mongoose.model('TypeManga', TypeMangaSchema);