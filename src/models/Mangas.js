const mongoose = require('mongoose');

const MangaSchema = new mongoose.Schema({
    name: { type: String, required: true },
    id_type_manga: { type: String, required: true },
    author: {type: String, required: true},
    url: {type: String, required: true}
})

module.exports.Mangas = mongoose.model('Manga', MangaSchema);